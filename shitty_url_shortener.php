<?php

ini_set('display_errors', '0');
header('Content-Type: text/plain');

global $shitty_url_storage;

$shitty_url_storage = getenv('SHITTY_URL_STORAGE');
if (!$shitty_url_storage)
    $shitty_url_storage = __DIR__ . '/shitty_url_storage.php';

define('SHITTY_URL_STORAGE', $shitty_url_storage);

if (!file_exists(SHITTY_URL_STORAGE))
    save(array());

function load()
{
    return include SHITTY_URL_STORAGE;
}

function save($shitty_urls)
{
    file_put_contents(
        SHITTY_URL_STORAGE,
        sprintf('<?php return %s;', var_export($shitty_urls, true))
    );
}

function auth()
{
    if (!$server = (string) getenv('SHITTY_URL_ADMIN'))
        return;

    $client = $_SERVER['PHP_AUTH_USER'] . ':' . $_SERVER['PHP_AUTH_PW'];
    if ($client !== $server) {
        header('HTTP/1.1 401 Unauthorized');
        die();
    }
}

function fullUrl($id)
{
    return sprintf(
        "https://%s/%s",
        $_SERVER['HTTP_HOST'],
        $id
    );
}


$id = trim(trim($_SERVER['REQUEST_URI']), '/');

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $urls = load();
        $url = $urls[$id];

        if ($url) {
            header("Location: $url");
            echo $url;
        } else {
            header('HTTP/1.1 404 Not Found');
            die('Not Found');
        }

        break;
    case 'POST':
    case 'PUT':
        auth();

        if (!$id)
            $id = bin2hex(random_bytes(8));

        save(array($id => file_get_contents('php://input')) + load());
        echo fullUrl($id);

        break;
    case 'OPTIONS':
        auth();

        foreach (load() as $id => $url) {
            printf("%s => %s\n", fullUrl($id), $url);
        }

        break;
    case 'DELETE':
        auth();

        $links = load();
        unset($links[$id]);
        save($links);

        break;
    default:
        header('HTTP/1.1 405 Method Not Allowed');
        die();
}
?>
